"""empty message

Revision ID: ded497c7993f
Revises: ae201208b2b7
Create Date: 2019-04-12 18:01:41.123685

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ded497c7993f'
down_revision = 'ae201208b2b7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('app',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('developer', sa.String(), nullable=True),
    sa.Column('icon', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('component',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('collection', sa.String(), nullable=True),
    sa.Column('type', sa.String(), nullable=True),
    sa.Column('layout', sa.Unicode(), nullable=True),
    sa.Column('options', sa.Unicode(), nullable=True),
    sa.Column('filters', sa.Unicode(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('permission',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=80), nullable=True),
    sa.Column('description', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('app_component',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('name', sa.Unicode(), nullable=True),
    sa.Column('template', sa.Unicode(), nullable=True),
    sa.Column('layout', sa.Unicode(), nullable=True),
    sa.Column('options', sa.Unicode(), nullable=True),
    sa.Column('filters', sa.Unicode(), nullable=True),
    sa.Column('app_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['app_id'], ['app.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('app_view',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('name', sa.Unicode(), nullable=True),
    sa.Column('layout', sa.Unicode(), nullable=True),
    sa.Column('options', sa.Unicode(), nullable=True),
    sa.Column('filters', sa.Unicode(), nullable=True),
    sa.Column('app_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['app_id'], ['app.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('context',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('name', sa.Unicode(), nullable=True),
    sa.Column('filters', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('model_definition',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('namespace', sa.Unicode(), nullable=True),
    sa.Column('name', sa.Unicode(), nullable=True),
    sa.Column('inherits', sa.Unicode(), nullable=True),
    sa.Column('next_model_id', sa.Integer(), nullable=True),
    sa.Column('field_01_label', sa.Unicode(), nullable=True),
    sa.Column('field_01_type', sa.String(), nullable=True),
    sa.Column('field_01_options', sa.Unicode(), nullable=True),
    sa.Column('field_02_label', sa.Unicode(), nullable=True),
    sa.Column('field_02_type', sa.String(), nullable=True),
    sa.Column('field_02_options', sa.Unicode(), nullable=True),
    sa.Column('field_03_label', sa.Unicode(), nullable=True),
    sa.Column('field_03_type', sa.String(), nullable=True),
    sa.Column('field_03_options', sa.Unicode(), nullable=True),
    sa.Column('field_04_label', sa.Unicode(), nullable=True),
    sa.Column('field_04_type', sa.String(), nullable=True),
    sa.Column('field_04_options', sa.Unicode(), nullable=True),
    sa.Column('field_05_label', sa.Unicode(), nullable=True),
    sa.Column('field_05_type', sa.String(), nullable=True),
    sa.Column('field_05_options', sa.Unicode(), nullable=True),
    sa.Column('field_06_label', sa.Unicode(), nullable=True),
    sa.Column('field_06_type', sa.String(), nullable=True),
    sa.Column('field_06_options', sa.Unicode(), nullable=True),
    sa.Column('field_07_label', sa.Unicode(), nullable=True),
    sa.Column('field_07_type', sa.String(), nullable=True),
    sa.Column('field_07_options', sa.Unicode(), nullable=True),
    sa.Column('field_08_label', sa.Unicode(), nullable=True),
    sa.Column('field_08_type', sa.String(), nullable=True),
    sa.Column('field_08_options', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('notification',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('deleted', sa.Boolean(), nullable=True),
    sa.Column('deleted_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('message_template', sa.String(), nullable=True),
    sa.Column('message_context', sa.JSON(), nullable=True),
    sa.Column('url', sa.String(), nullable=True),
    sa.Column('schedule_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('is_fired', sa.Boolean(), nullable=True),
    sa.Column('fired_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('is_read', sa.Boolean(), nullable=True),
    sa.Column('read_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('roles_permissions',
    sa.Column('permission_id', sa.Integer(), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['permission_id'], ['permission.id'], ),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], )
    )
    op.create_table('user_installed_app',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('app_id', sa.Integer(), nullable=True),
    sa.Column('enabled', sa.Boolean(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['app_id'], ['app.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('view',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('name', sa.Unicode(), nullable=True),
    sa.Column('route', sa.Unicode(), nullable=True),
    sa.Column('layout', sa.Unicode(), nullable=True),
    sa.Column('options', sa.Unicode(), nullable=True),
    sa.Column('filters', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('model_instance',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('model_definition_id', sa.Integer(), nullable=True),
    sa.Column('model_id', sa.Integer(), nullable=True),
    sa.Column('field_01_value', sa.Unicode(), nullable=True),
    sa.Column('field_02_value', sa.Unicode(), nullable=True),
    sa.Column('field_03_value', sa.Unicode(), nullable=True),
    sa.Column('field_04_value', sa.Unicode(), nullable=True),
    sa.Column('field_05_value', sa.Unicode(), nullable=True),
    sa.Column('field_06_value', sa.Unicode(), nullable=True),
    sa.Column('field_07_value', sa.Unicode(), nullable=True),
    sa.Column('field_08_value', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['model_definition_id'], ['model_definition.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('relationship_definition',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('from_model_definition_id', sa.Integer(), nullable=True),
    sa.Column('to_model_definition_id', sa.Integer(), nullable=True),
    sa.Column('relationship_name', sa.Unicode(), nullable=True),
    sa.Column('relationship_backref', sa.Unicode(), nullable=True),
    sa.Column('relationship_type', sa.String(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['from_model_definition_id'], ['model_definition.id'], ),
    sa.ForeignKeyConstraint(['to_model_definition_id'], ['model_definition.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user_settings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('last_context_id', sa.Integer(), nullable=True),
    sa.Column('background_img_url', sa.String(), nullable=True),
    sa.Column('contexts_enabled', sa.Boolean(), nullable=True),
    sa.Column('search_enabled', sa.Boolean(), nullable=True),
    sa.Column('notifications_enabled', sa.Boolean(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['last_context_id'], ['context.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user_view',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('icon', sa.String(), nullable=True),
    sa.Column('on_main', sa.Boolean(), nullable=True),
    sa.Column('order', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('context_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['context_id'], ['context.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('relationship_instance',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('updated_ts', sa.DateTime(timezone=True), nullable=True),
    sa.Column('relationship_definition_id', sa.Integer(), nullable=True),
    sa.Column('from_model_instance_id', sa.Integer(), nullable=True),
    sa.Column('to_model_instance_id', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['from_model_instance_id'], ['model_instance.id'], ),
    sa.ForeignKeyConstraint(['relationship_definition_id'], ['relationship_definition.id'], ),
    sa.ForeignKeyConstraint(['to_model_instance_id'], ['model_instance.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user_component',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('component_id', sa.Integer(), nullable=True),
    sa.Column('user_view_id', sa.Integer(), nullable=True),
    sa.Column('order', sa.Integer(), nullable=True),
    sa.Column('columns', sa.Integer(), nullable=True),
    sa.Column('options', sa.JSON(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['component_id'], ['component.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.ForeignKeyConstraint(['user_view_id'], ['user_view.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('user_component')
    op.drop_table('relationship_instance')
    op.drop_table('user_view')
    op.drop_table('user_settings')
    op.drop_table('relationship_definition')
    op.drop_table('model_instance')
    op.drop_table('view')
    op.drop_table('user_installed_app')
    op.drop_table('roles_permissions')
    op.drop_table('notification')
    op.drop_table('model_definition')
    op.drop_table('context')
    op.drop_table('app_view')
    op.drop_table('app_component')
    op.drop_table('permission')
    op.drop_table('component')
    op.drop_table('app')
    # ### end Alembic commands ###
