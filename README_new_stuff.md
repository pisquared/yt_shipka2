```bash
# 0. install docker

# 0. install gcloud 
wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-239.0.0-linux-x86_64.tar.gz

# 0. install gsutil
wget https://storage.googleapis.com/pub/gsutil.tar.gz

# 2. install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.0/bin/linux/amd64/kubectl
chmod +x ./kubectl

##################################
# INSTALL MINIKUBE
##################################

# 1. download minikube
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo mv minikube /usr/local/bin
chmod +x /usr/local/bin/minikube

# 3. start minikube with higher reqs for istio
minikube start --memory=8192 --cpus=2 --kubernetes-version=v1.13.0 --insecure-registry "10.0.0.0/24"

# 4. minikube DNS is broken (of course...) - manually inject kube-dns clusterip into /etc/resolv.conf
DNS=$(kubectl get service/kube-dns --namespace kube-system --template '{{.spec.clusterIP}}')
minikube ssh "echo -e \"[Resolve]\nDNS=$DNS\" | sudo tee /etc/systemd/resolved.conf"
minikube ssh "sudo systemctl restart systemd-resolved --wait"

##################################
# INSTALL KUBE_REGISTRY - local docker registry
##################################

# 5. run a local registry 
# https://blog.hasura.io/sharing-a-local-registry-for-minikube-37c7240d0615/
# editing the kube_registry with higher limits
# to limit how many concurrent connections, add --max-concurrent-uploads flag of the /lib/systemd/system/docker.service
cd _kube_registry
kubectl create namespace registry
kubectl create -f kube-registry.yaml
kubectl port-forward --namespace registry $(kubectl get po -n registry | grep kube-registry-v0 | \awk '{print $1;}') 7001:7001
cd ..

# https://ryaneschinger.com/blog/using-google-container-registry-gcr-with-minikube/
# Run own registry
# https://docs.docker.com/registry/deploying/
# docker run -d -p 7000:5000 --restart=always --name registry registry:2

##################################
# GOOGLE CLOUD PLATFORM BOOKINFO
##################################

# 5. install gcp bookshelf app
# 5.1 create a Google Cloud Storage bucket
gsutil mb gs://pi2ytdl
gsutil defacl set public-read gs://pi2ytdl

# 5.2 build the docker image and push to local repo
docker build -t localhost:7001/bookshelf .
docker push localhost:7001/bookshelf

# 5.3 install the service account secret, the frontend and worker deployments, and the frontend service
kubectl create namespace bookshelf
kubectl create secret generic google-application-credentials-secret --from-file=./pi2ytdl-svc.json -n bookshelf
kubectl apply -f bookshelf-frontend.yaml
kubectl apply -f bookshelf-worker.yaml 
kubectl apply -f bookshelf-service.yaml

# 5.4 to access service - basically giving shortcut to cluster_ip:nodeport
minikube service bookshelf-frontend

##################################
# ISTIO
##################################

# 6. download istio
curl -L "https://github.com/istio/istio/releases/download/1.1.1/istio-1.1.1-linux.tar.gz" | tar xz
cd istio-1.1.1
for i in install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done
kubectl apply -f install/kubernetes/istio-demo.yaml

# 7. create a namespace for istio bookinfo istio example app
kubectl create namespace bookinfo-istio
kubectl label namespace bookinfo-istio istio-injection=enabled

# 7.1. create the example bookinfo istio and the gateway
kubectl apply -n bookinfo-istio -f samples/bookinfo/platform/kube/bookinfo.yaml
kubectl apply -n bookinfo-istio -f samples/bookinfo/networking/bookinfo-gateway.yaml

# 7.2. construct gateway url
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
export INGRESS_HOST=$(minikube ip)

export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT

# 7.3 apply destination rule
kubectl apply -f samples/bookinfo/networking/destination-rule-all.yaml

##################################
# OPTIONAL:::: WEB-gRPC
##################################

# 8. download protoc and protoc-gen-grpc-web
wget https://github.com/protocolbuffers/protobuf/releases/download/v3.7.0/protoc-3.7.0-linux-x86_64.zip
unzip -d protoc protoc-3.7.0-linux-x86_64.zip
wget https://github.com/grpc/grpc-web/releases/download/1.0.4/protoc-gen-grpc-web-1.0.4-linux-x86_64

# 9. "install" protoc and protoc-gen-grpc-web to system
sudo mv protoc/bin/protoc /usr/local/bin/protoc
sudo mv protoc/include /usr/local/include/protos
sudo mv protoc-gen-grpc-web-1.0.4-linux-x86_64 /usr/local/bin/protoc-gen-grpc-web
chmod +x /usr/local/bin/protoc-gen-grpc-web

# 10. compile protos
protoc -I=. helloworld.proto \
  --js_out=import_style=commonjs:. \
  --grpc-web_out=import_style=commonjs,mode=grpcwebtext:.

# 11. install npm packages
npm install

# 12. webpack the client
npx webpack client.js

# 13. run the node server -- running on 9090
node server.js

# 14. build the envoy proxy -- running on 8080
docker build -t helloworld/envoy -f ./envoy.Dockerfile .
docker run -d -p 8080:8080 --name=envoy --network=host helloworld/envoy

# 15. run a python server on 8081 - serving index.html and dist/main.j
python3 -m http.server 8081

# open browser on 8081 and see dev console
```