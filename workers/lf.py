# -*- coding:utf-8 -*-
import requests
import shutil
from pprint import pprint

lf_api_key = "831834c8d4cd5d3e4821f565ab4f9a75"
lf_shared_secret = "d5b3cd605fd700d6cdf11ee677dc0c70"
root_url = "http://ws.audioscrobbler.com/2.0/"

#resp = requests.get("{url}?method=auth.gettoken&api_key={api_key}&format=json".format(url=root_url, api_key=lf_api_key))
#print(resp.json())

token = "uGJPE5Rp9-7bQevUb5dUkJ8uYHGvbRCc"

resolved_genres = ["rock", "folk", "jazz", "blues", "pop", "classical", "country", "rhythm and blues", "popular", "reggae", "heavy metal", "metal", "punk rock", "soul", "electronic", "singing", "alternative", "techno", "funk", "dance", "house", "psychedelic", "ambient", "disco", "electro", "progressive", "instrumental", "trance", "breakbeat", "orchestra", "world", "industrial", "progressive", "dubstep", "pop rock", "baroque", "gospel", "art", "grunge", "soundtrack", "chant", "indie rock", "dub", "hard rock", "drum and bass", "hardstyle", "hardcore", "balad", "classic rock", "rnb", "rap", "punk", "latino", "epic", "xmas"]

normalized_genres = {
        "classic": "classical",
        "classic music": "classical",
        "hip-hop": "rap",
        "hiphop": "rap",
        "hip hop": "rap",
        "ballad": "balad",
        "ost": "soundtrack",
        "christmas": "xmas",
        "christmas music": "xmas",
        "christmas songs": "xmas",
        "christmas oldies": "xmas",
    }

resolved_decades = ["00s", "10s", "20s", "30s", "40s", "50s", "60s", "70s", "80s", "90s"]

resolved_languages = ["bulgairan", "german", "russian", "italian", "french", "english", "greek", "finnish", "romanian", "serbian", "norwegian", "spannish", "swedish", "scottish", "hungarian"]


# TODO:
normalized_languages = {
  "bg": "bulgarian",
  "български": "bulgarian",
  "magyar": "hungarian",
  "magyarok": "hungarian",
  "руски": "russian",
  "germany": "german",
}

resolved_moods = ["happy", "sad", "funny", "romantic", ]


def search_last_fm(track_q):
    search_resp = requests.get(root_url, params={
        "method": "track.search",
        "track": track_q,
        "api_key": lf_api_key,
        "format": "json",
    })

    if search_resp.status_code != 200:
        import ipdb; ipdb.set_trace()
        return {}

    track_results = search_resp.json().get("results", {}).get("trackmatches", {}).get("track", [])
    return track_results


def track_meta_last_fm(track_result):
    track_name = track_result.get("name")
    track_artist = track_result.get("artist")
    track_mbid = track_result.get("mbid")

    params = {
        "method": "track.getInfo",
        "api_key": lf_api_key,
        "format": "json",
    }
    if track_mbid:
        params["mbid"] = track_mbid
    else:
        params["track"], params["artist"] = track_name, track_artist

    track_info_resp = requests.get(root_url, params=params)
    
    if track_info_resp.status_code != 200:
        import ipdb; ipdb.set_trace()
        return {}
    
    track_info = track_info_resp.json().get("track", "")
    track_mbid = track_info.get("mbid", "")
    song_name = track_info.get("name", "")
    artist = track_info.get("artist", {}).get("name", "")
    artist_mbid = track_info.get("artist", {}).get("mbid", "")
    album_name = track_info.get("album", {}).get("title", "")
    album_mbid = track_info.get("album", {}).get("mbid", "")
    pprint(track_info)
    
    tags, genres, decades, languages, moods = [], [], [], [], []
    tags_meta = track_info.get("toptags", {}).get("tag", [])
    for tag_meta in tags_meta:
        tag = tag_meta.get("name")
        tag_lower = tag.lower()
        if tag_lower in resolved_genres:
            genres.append(tag_lower)
        elif tag_lower in normalized_genres:
            genres.append(normalized_genres.get(tag_lower))
        elif tag_lower in resolved_decades:
            decades.append(tag_lower)
        elif tag_lower in resolved_languages:
            languages.append(tag_lower)
        elif tag_lower in resolved_moods:
            moods.append(tag_lower)
        elif tag_lower == artist.lower() or tag_lower == song_name.lower() or tag_lower == album_name.lower():
            continue
        else:
            tags.append(tag)
    
    image_url = ""
    images_meta = track_info.get("album", {}).get("image", [])
    for image_meta in images_meta:
        image_url = image_meta.get("#text")
        
    return {
        "title": song_name,
        "artist": artist,
        "album": album_name,
        "track_mbid": track_mbid,
        "artist_mbid": artist_mbid,
        "album_mbid": album_mbid,
        "track_number": track_info.get("album", {}).get("@attr", {}).get("position"),
        "album_image_url": image_url,
        "genres": genres,
        "tags": tags,
        "decades": decades,
        "languages": languages,
        "moods": moods,
        "last_fm_played_count": track_info.get("playcount"),
    }


def search_and_meta_last_fm(track_q):
    track_results = search_last_fm(track_q)

    if len(track_results):
        track_result = track_results[0]
        return track_meta_last_fm(track_result)
        
    return {}
