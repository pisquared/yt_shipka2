import yaml

with open("models.yml", 'r') as stream:
    for model in yaml.safe_load_all(stream):
        print(model)
