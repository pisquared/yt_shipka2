"""
Spotify API: https://developer.spotify.com/documentation/web-api/reference/tracks/get-audio-features/
"""

import requests
import os
from base64 import b64encode
from datetime import datetime, timedelta
from flask import current_app
CLIENT_ID = "cf05fcc582354d9e8b8bafc05d5651f3"

"""
https://developer.spotify.com/dashboard/
"""
SPOTIFY_CLIENT_SECRET_FILE = "spotify_client_secret.secret"
CLIENT_SECRET = current_app.config.get("spotify_client_secret")

AUTH_URL = "https://accounts.spotify.com/api/token"
SEARCH_URL = "https://api.spotify.com/v1/search"
AUDIO_FEATURES_URL = "https://api.spotify.com/v1/audio-features/{id}"

SPOTIFY_ACCESS_TOKEN_FILE = "spotify_access_token.secret"


def authorize():
    """
    https://developer.spotify.com/documentation/general/guides/authorization-guide/
    :return:
    """
    base_64_auth = b64encode(
        "{}:{}".format(CLIENT_ID, CLIENT_SECRET).encode("utf-8")).decode("utf-8")
    rr = requests.post(AUTH_URL,
                       data={
                           "grant_type": "client_credentials",
                       },
                       headers={
                           "Authorization": "Basic {}".format(base_64_auth),
                       })
    if rr.status_code == 200:
        response = rr.json()
        access_token = response.get("access_token")
        expires_in = response.get("expires_in")
        if expires_in and str(expires_in).isdigit():
            return access_token, expires_in
    else:
        import pdb;
        pdb.set_trace()


def get_access_token():
    now = datetime.utcnow()
    if os.path.exists(SPOTIFY_ACCESS_TOKEN_FILE):
        with open(SPOTIFY_ACCESS_TOKEN_FILE) as f:
            contents = f.read()
            if contents:
                access_token, expires_in = contents.split("#")
                expire_dt = datetime.strptime(expires_in, '%Y-%m-%dT%H:%M:%S')
                if expire_dt > now:
                    return access_token
    access_token, expires_in = authorize()
    if access_token:
        with open(SPOTIFY_ACCESS_TOKEN_FILE, "w") as f:
            expire_dt = now + timedelta(seconds=expires_in)
            f.write(
                "{}#{}".format(access_token, expire_dt.strftime("%Y-%m-%dT%H:%M:%S")))
    return access_token


def spotify_audio_features(song_id, access_token=""):
    """
    https://developer.spotify.com/console/get-audio-features-track/
    :param song_id:
    :param access_token:
    :return:
    """
    if not song_id:
        return
    rr = requests.get(AUDIO_FEATURES_URL.format(id=song_id),
                      headers={
                          "Authorization": "Bearer {}".format(access_token)
                      })
    if rr.status_code == 200:
        results = rr.json()
        return results
    else:
        import pdb;
        pdb.set_trace()


def spotify_search(q, search_type="track", access_token="", limit=1):
    """
    https://developer.spotify.com/documentation/web-api/reference/search/search/
    :param q:
    :param search_type:
    :param access_token:
    :param limit:
    :return:
    """
    rr = requests.get(SEARCH_URL,
                      headers={
                          "Authorization": "Bearer {}".format(access_token)
                      },
                      params={
                          "q": q,
                          "type": search_type,
                          "limit": limit,
                      })
    if rr.status_code == 200:
        results = rr.json()
        songs = results.get("tracks", {}).get("items", [])
        for song in songs:
            song_id = song.get("id")
            return song_id
    else:
        import pdb;
        pdb.set_trace()


def get_song_features(q):
    access_token = get_access_token()
    song_id = spotify_search(q=q, access_token=access_token)
    features = spotify_audio_features(song_id=song_id, access_token=access_token)
    return features


if __name__ == "__main__":
    print(get_song_features("Eminem lose yourself"))
