function updateURLParameter(url, param, paramVal) {
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] != param) {
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}


var audioEl = document.getElementById('audio');
var audioStartEl = document.getElementById('audio-start');
var audioStartTime = audioStartEl.value;
var audioEndEl = document.getElementById('audio-end');
var audioEndTime = audioEndEl.value;

function updateStart(val) {
    audioStartEl.value = val;
    audioStartTime = val;
    window.history.replaceState('', '', updateURLParameter(window.location.href, "s", audioStartTime));
}

function updateEnd(val) {
    audioEndEl.value = val;
    audioEndTime = val;
    window.history.replaceState('', '', updateURLParameter(window.location.href, "e", audioEndTime));
}

audioEl.addEventListener('canplaythrough', function () {
    if (this.currentTime < audioStartTime) {
        this.currentTime = audioStartTime;
    }
    if (this.currentTime > audioEndTime) {
        this.currentTime = audioStartTime;
    }

    this.play();
});

var shouldLoop = $('#should-loop').prop("checked");
$('#should-loop').change(function () {
    shouldLoop = $('#should-loop').prop("checked");
})
var redirected = false;
audioEl.addEventListener('timeupdate', function () {
    console.log("update");
    if (this.currentTime < audioStartTime) {
        this.currentTime = audioStartTime;
    }
    if (this.currentTime > audioEndTime - 1) {
        console.log(audioEndTime - this.currentTime);
        if (shouldLoop) {
            this.currentTime = audioStartTime;
        } else {
            if (!redirected) {
                var recEl = $('#recommendation-idx-1');
                if (recEl) {
                    var nextAddress = recEl.attr('href');
                    console.log(nextAddress);
                    redirected = true;
                    window.location.href = window.location.origin + nextAddress;

                }
            }
        }
    }
});

audioStartEl.addEventListener('change', function () {
    updateStart(audioStartEl.value);
});

audioEndEl.addEventListener('change', function () {
    updateEnd(audioEndEl.value)
});

var fromStartBtnEl = document.getElementById('audio-btn-from-start');
var tilEndBtnEl = document.getElementById('audio-btn-til-end');


fromStartBtnEl.addEventListener('click', function () {
    updateStart(0);
});

var mediaLength = audioEndTime;

fromStartBtnEl.addEventListener('click', function () {
    updateEnd(mediaLength)
    ;
});


function toggle(elId) {
    var x = document.getElementById(elId);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

$('#recommendation-idx-1');