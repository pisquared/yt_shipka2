import datetime
import shutil

import os

import re
from collections import defaultdict

import requests
from shipka.store.database import db
from shipka.webapp.util import get_user
from shipka.workers import celery

from webapp.api.models import Label, Song
from workers.spotify import get_song_features
from workers.lf import search_and_meta_last_fm

STOPWORDS = [
    "lyrics", "hd"
]


@celery.task
def sum_args(x, y):
    return str(x + y)


def compare_titles(this_song_artist, this_song_title, other_song):
    other_song_artist = other_song.label_key('artist')
    other_song_title = other_song.label_key('title')
    if not (other_song_artist and other_song_title):
        return False
    return this_song_artist.lower() == other_song_artist.lower() and \
           this_song_title.lower() == other_song_title.lower()


def log_song_played(song):
    now = str(datetime.datetime.utcnow())
    play_count_label, last_played_label = None, None
    for label in song.labels:
        if label.key == 'last_played':
            last_played_label = label
        if label.key == 'play_count':
            play_count_label = label
    if not play_count_label:
        play_count_label = Label.create(key="play_count", value="1", indexing=False)
    else:
        prev_play_count = play_count_label.value
        if prev_play_count and prev_play_count.isdigit():
            prev_play_count = int(prev_play_count)
            play_count_label.value = str(prev_play_count + 1)
            db.session.add(play_count_label)
    if not last_played_label:
        last_played_label = Label.create(key="last_played",
                                         value=now,
                                         indexing=False,
                                         user=get_user(),
                                         )
    else:
        last_played_label.value = now
    song.labels.append(play_count_label)
    song.labels.append(last_played_label)
    db.session.add(song)
    db.session.commit()


@celery.task
def duplicate_from_song(this_song):
    this_song_duplicates = this_song.label_key('duplicate_ids')
    duplicates = []
    if this_song_duplicates:
        duplicate_ids = this_song_duplicates.split()
        try:
            for dupl_id in duplicate_ids:
                song = Song.query.filter_by(id=dupl_id).first()
                if song:
                    duplicates.append(song)
        except:
            # TODO: raise exception
            pass
        return duplicates
    this_song_artist = this_song.label_key('artist')
    this_song_title = this_song.label_key('title')
    if not (this_song_artist and this_song_title):
        return []

    all_songs = Song.query.all()
    for other_song in all_songs:
        if this_song == other_song:
            continue
        if compare_titles(this_song_artist, this_song_title, other_song):
            duplicates.append(other_song)
    if not duplicates:
        this_song.labels.append(Label.create(key="duplicate_ids",
                                             value='-1',
                                             indexing=False,
                                             user=get_user(),
                                             ))
    else:
        this_song.labels.append(Label.create(key="duplicate_ids",
                                             value=' '.join([str(x.id) for x in duplicates]),
                                             indexing=False,
                                             user=get_user(),
                                             ))
    this_song.labels.append(Label.create(key="duplicate_calc_dt",
                                         value=str(datetime.datetime.utcnow()),
                                         indexing=False,
                                         user=get_user(),
                                         ))
    db.session.add(this_song)
    db.session.commit()
    return duplicates


MAX_LIST = 15


@celery.task
def recommend_from_song(this_song, song_duplicates):
    recomended_songs = defaultdict(lambda: {
        'song_id': 0,
        'score': 0,
        'last_played': 0,
        'reasons': defaultdict(int)
    })
    # TODO: This could be optimized with joins
    for label in this_song.indexed_labels:
        other_labels = Label.query.filter_by(key=label.key, value=label.value).all()
        for other_label in other_labels:
            for other_song in other_label.songs:
                if this_song == other_song or other_song in song_duplicates:
                    continue
                last_played = other_song.label_key('last_played')
                recomended_songs[other_song.id]['song_id'] = other_song.id
                recomended_songs[other_song.id]['title'] = other_song.title
                recomended_songs[other_song.id]['score'] += 1
                recomended_songs[other_song.id]['last_played'] = last_played or '0'
                recomended_songs[other_song.id]['reasons'][label.key] += 1
    # sort MAX_LIST number of high-scoring songs, then rev sort by last played
    score_cut = sorted(recomended_songs.values(), key=lambda x: -x['score'])[:MAX_LIST]
    return sorted(score_cut, key=lambda x: x['last_played'])


def create_label(song, key, value, indexing):
    if value:
        label = Label.create(key=key, value=value, indexing=indexing, user=get_user())
        song.labels.append(label)


def process_last_fm_resp(r, song, next_song_id, g_artist, g_title, g_album):
    last_fm_artist = r.get("artist")
    last_fm_title = r.get("title")
    last_fm_album = r.get("album")

    if g_artist:
        if g_artist != last_fm_artist:
            create_label(song, "last_fm_artist", last_fm_artist, False)
    else:
        create_label(song, "artist", last_fm_artist, True)

    if g_title:
        if g_title != last_fm_title:
            create_label(song, "last_fm_title", last_fm_title, False)
    else:
        create_label(song, "title", last_fm_title, False)

    create_label(song, "album", last_fm_album, True)

    if g_album != last_fm_album:
        create_label(song, "album", g_album, True)

    create_label(song, "track_mbid", r.get("track_mbid"), False)
    create_label(song, "artist_mbid", r.get("artist_mbid"), False)
    create_label(song, "album_mbid", r.get("album_mbid"), False)
    create_label(song, "track_number", r.get("track_number"), False)
    create_label(song, "last_fm_play_count", r.get("last_fm_played_count"), False)
    for genre in r.get("genres"):
        create_label(song, "genre", genre, True)
    for tag in r.get("tags"):
        create_label(song, "tag", tag, True)
    for decade in r.get("decades"):
        create_label(song, "decade", decade, True)
    for language in r.get("languages"):
        create_label(song, "language", language, True)
    for mood in r.get("moods"):
        create_label(song, "mood", mood, True)
    album_image_url = r.get("album_image_url")
    if album_image_url:
        try:
            path = os.path.join(basepath, "media", "{}_album.png".format(next_song_id))
            r = requests.get(album_image_url, stream=True)
            if r.status_code == 200:
                with open(path, 'wb') as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)
        except:
            pass


def clean_yt_title(song_title):
    """
    match all the letter-like characters
    """
    clean_title = re.sub(r'\([^)]*\)|\[[^]]*\]', '', song_title)
    clean_title = re.findall("[\w]+'[\w]+|\w+|-", clean_title, re.UNICODE)
    clean_title = [w for w in clean_title if w.lower() not in STOPWORDS]
    return ' '.join(clean_title)


def enrich_song_from_yt_title_to_last_fm(song, next_song_id):
    clean_title = clean_yt_title(song.title)
    song.title = clean_title
    track_meta = search_and_meta_last_fm(clean_title)
    process_last_fm_resp(track_meta, song, next_song_id, None, None, None)


def enrich_song_from_spotify(song):
    q = song.title
    song_features = get_song_features(q)
    tempo = song_features.get("tempo")
    if tempo:
        bpm_round = int(tempo)
        bpm_round_10 = int((tempo // 10) * 10)
        create_label(song, "bpm_round", str(bpm_round), True)
        create_label(song, "bpm_round_10", str(bpm_round_10), True)

    spotify_id = song_features.get("id")
    create_label(song, "spotify_id", spotify_id, False)
    for k in ["key", "mode", "time_signature", "acousticness", "danceability",
              "energy", "instrumentalness", "liveness", "loudness",
              "speechiness", "valence"]:
        spotify_k = song_features.get(k)
        if spotify_k is not None:
            create_label(song, "spotify_{}".format(k), str(spotify_k), False)
