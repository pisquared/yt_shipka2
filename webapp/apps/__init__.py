import json
import os
from flask import Blueprint

from shipka.webapp.registries import COMPONENT_REGISTRY

apps = Blueprint('apps', __name__,
                 static_folder='components',
                 static_url_path='/static/apps',
                 template_folder='components')

# register app static assets
apps_dir = os.path.join(os.getcwd(), 'apps')
for _, dirs, _ in os.walk(apps_dir):
    for dir in dirs:
        app_dir = os.path.join(apps_dir, dir)
        manifest = json.load(open(os.path.join(app_dir, 'manifest.json')))
        app_name = dir
        # create a symlink to components from webapp
        components_dir = os.path.join(app_dir, 'components')
        symlink_dir = os.path.join('webapp', 'apps', 'components', dir)
        if not os.path.exists(symlink_dir):
            cwd = os.getcwd()
            source = os.path.join(cwd, components_dir)
            dest = os.path.join(cwd, symlink_dir)
            try:
                os.symlink(source, dest)
            except OSError:
                pass
        components = manifest['components']
        # write to component registry from the manifest
        for component in components:
            component_name = component['name']
            COMPONENT_REGISTRY[component_name] = component
            COMPONENT_REGISTRY[component_name]['app_name'] = app_name
            for external_asset in ['external_scripts']:
                external_values = component.get(external_asset, [])
                COMPONENT_REGISTRY[component_name][external_asset] = [ev for ev in external_values]
            for asset in ['templates', 'scripts', 'styles', 'img']:
                COMPONENT_REGISTRY[component_name][asset] = []
                asset_dir = os.path.join(components_dir, component_name, asset)
                for _, _, filenames in os.walk(asset_dir):
                    # TODO: this is level 5 of for - try to optimize
                    COMPONENT_REGISTRY[component_name][asset] += filenames
                    break
    break
