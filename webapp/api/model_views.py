"""
Define view_models like so:

```
MODELS_REGISTRY.update({
    'blog_post': {
        'vm': ModelView,
        'model': BlogPost,
        # The ones below are optional
        form_include: [],
        form_exclude: [],
        form_only: [],
        creatable: True,
        editable: True,
        deletable: True,
        user_needed: True,
        creatable_login_required: ['admin', ]
        'creatable_roles_required': ['admin', ]
    },
})
```
"""

from shipka.webapp.registries import MODELS_REGISTRY, update_model_registry
from shipka.webapp.util import ModelView, get_user

from webapp.api import api
from webapp.api.models import Song, Label


class LabelModelView(ModelView):
    def handle_get_filters(self, filters):
        retrieve_filters = dict(
            user=get_user()
        )
        if 'song_id' in filters:
            retrieve_filters['id'] = filters['song_id']
        song = Song.get_one_by(**retrieve_filters)
        if not song:
            return []
        return song.labels


MODELS_REGISTRY.update({
    'song': {
        'vm': ModelView,
        'model': Song,
    },
    'label': {
        'vm': LabelModelView,
        'model': Label,
    },
})

update_model_registry(api)
