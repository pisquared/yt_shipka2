import json

import os
import urlparse
from collections import defaultdict

from flask import render_template, flash, redirect, url_for, request, send_from_directory
from flask_login import current_user, login_required
from shipka.webapp.util import get_user
from shipka.webapp.views.crudy_integration import build_ctx
from shipka.webapp import media
from shipka.store.database import db

from webapp.api.models import Label, Song
from webapp.client import client
from workers.lf import search_last_fm
from workers.tasks import duplicate_from_song, recommend_from_song, log_song_played, \
    enrich_song_from_yt_title_to_last_fm, enrich_song_from_spotify

YOUTUBE_PREFIXES = [
    "https://www.youtube.com/watch",
    "https://m.youtube.com/watch",
]


def build_local_ctx():
    ctx = build_ctx()
    ctx['title'] = 'Music'
    ctx['project_settings'] = {'search_enabled': True}
    ctx['user'] = current_user
    ctx['q'] = request.args.get('q', '')
    ctx['user_views'] = [
        {
            'id': 1,
            'on_main': True,
            'name': "Songs",
        }
    ]
    return ctx


@login_required
@client.route('/')
def get_home():
    ctx = build_local_ctx()
    return render_template('index.html',
                           **ctx)


@client.route('/views/<int:user_view_id>')
@login_required
def get_view(user_view_id):
    user = get_user()
    user_view = UserView.get_one_by(user=user, id=user_view_id)
    if not user_view:
        flash('Error getting view id {}.'.format(user_view_id))
        return redirect('/')
    ctx = build_local_ctx()
    ctx['user_view'] = user_view
    ctx['view'] = view
    return render_template('view.html',
                           **ctx)


@login_required
@client.route("/all-songs")
def all_songs():
    ctx = build_local_ctx()
    return render_template("all_songs.html",
                           **ctx)


@login_required
@client.route("/labels")
def labels():
    ctx = build_local_ctx()
    label_key = request.args.get('k')
    label_value = request.args.get('v')
    all_songs, label_keys, label_values = [], [], []
    if label_key and label_value:
        labels = Label.query.filter_by(key=label_key, value=label_value).all()
        all_songs = []
        for label in labels:
            for song in label.songs:
                all_songs.append(song)
    elif label_key:
        label_vs = Label.query.with_entities(Label.value).filter_by(key=label_key).group_by(Label.value).all()
        label_values = [label_v[0] for label_v in label_vs]
    else:
        label_ks = Label.query.with_entities(Label.key).group_by(Label.key).all()
        label_keys = [label_k[0] for label_k in label_ks]
    return render_template("label.html",
                           label_key=label_key,
                           label_value=label_value,
                           all_songs=all_songs,
                           label_keys=label_keys,
                           label_values=label_values,
                           **ctx)


@login_required
@client.route("/listen/<int:song_id>")
def listen(song_id):
    ctx = build_local_ctx()
    song = Song.query.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(url_for('index'))
    media_st = int(request.args.get("s", song.default_start or 0))
    media_st = max(media_st, 0)
    media_duration = int(song.duration or 0)
    media_et = int(request.args.get("e", song.default_end or media_duration))
    media_et = min(media_et, media_duration)
    song_duplicates = duplicate_from_song(song)
    song_recomendations = recommend_from_song(song, song_duplicates)
    log_song_played(song)
    ctx["song"] = song
    ctx["media_fname"] = song.filename
    ctx["media_st"] = media_st
    ctx["media_et"] = media_et
    ctx["media_length"] = media_duration
    ctx["song_recomendations"] = song_recomendations
    ctx["song_duplicates"] = song_duplicates
    return render_template("listen.html",
                           **ctx)


@login_required
@client.route("/search")
def search():
    ctx = build_local_ctx()
    q = request.args.get("q")
    if not q:
        return redirect(request.referrer)
    q = q.strip()
    video_id = None
    for prefix in YOUTUBE_PREFIXES:
        if q.startswith(prefix):
            parsed = urlparse.urlparse(q)
            video_id = urlparse.parse_qs(parsed.query).get('v', [None])[0]
            break
    labels_by_youtube_video_id = Label.query.filter_by(key="youtube_video_id", value=video_id).all()
    youtube_songs = []
    for label in labels_by_youtube_video_id:
        for song in label.songs:
            youtube_songs.append(song)
    if len(youtube_songs) == 1:
        return redirect(url_for('client.listen', song_id=youtube_songs[0].id))
    if video_id:
        return render_template('search_results.html',
                               download_youtube_video=video_id,
                               youtube_songs=youtube_songs,
                               **ctx)
    song_results = []
    pre_song_results = Song.query.whooshee_search(q).all()
    for song in pre_song_results:
        if song not in youtube_songs:
            song_results.append(song)
    labels = Label.query.whooshee_search(q).all()
    label_results = defaultdict(list)
    for label in labels:
        for song in label.songs:
            if song in song_results:
                continue
            label_results["{}: {}".format(label.key, label.value)].append(song)

    return render_template('search_results.html',
                           download_youtube_video=video_id,
                           youtube_songs=youtube_songs,
                           label_results=label_results,
                           songs=song_results,
                           q=q,
                           )


class YdLogger(object):
    def __init__(self):
        self.info_json = {}

    def warning(self, msg):
        pass

    def error(self, msg):
        pass

    def debug(self, msg):
        try:
            self.info_json = json.loads(msg)
        except:
            print(msg)


@login_required
@client.route("/download")
def download():
    import youtube_dl
    from transliterate import translit
    q = request.args.get("q")
    song = Song.create(user=get_user())
    db.session.commit()
    try:
        next_song_id = song.id
        media_fpath = os.path.join(media.config.destination, str(next_song_id))
        yd_logger = YdLogger()
        ydl_opts = {
            'outtmpl': "{}.%(ext)s".format(media_fpath),
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }],
            'forcejson': True,
            'logger': yd_logger,
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download(['http://www.youtube.com/watch?v={}'.format(q)])
        song_title = yd_logger.info_json['title']
        song_duration = yd_logger.info_json['duration']
        # media_meta = MP3("{}.mp3".format(media_fpath))
        # song_length = int(media_meta.info.length)
        song.filename = "{}.mp3".format(next_song_id)
        song.duration = song_duration
        song.title = song_title
        enrich_song_from_yt_title_to_last_fm(song, next_song_id)
        enrich_song_from_spotify(song)
        youtube_label = Label.create(key="youtube_video_id",
                                     value=q,
                                     user=get_user(),
                                     )
        song.labels.append(youtube_label)

        try:
            indexing_title = translit(song_title, 'bg', reversed=True)
            if indexing_title:
                song.indexing_title = indexing_title
        except Exception as e:
            pass
    except Exception as e:
        flash("couldn't download song - {}".format(e))
        song.labels = []
        db.session.delete(song)
        db.session.commit()
        return redirect(request.referrer)
    db.session.add(song)
    db.session.commit()
    return redirect(url_for("client.listen", song_id=song.id))


@client.route("/songs/<int:song_id>/edit", methods=["post"])
def edit_song(song_id):
    song = Song.query.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referer)
    song.title = request.form.get('title')
    song.indexing_title = request.form.get('indexing_title')
    song.default_start = request.form.get('default_start')
    song.default_end = request.form.get('default_end')
    db.session.add(song)
    db.session.commit()
    return redirect(request.referrer)


@client.route("/songs/<int:song_id>/delete")
def delete_song(song_id):
    song = Song.query.filter_by(id=song_id).first()
    song_file = os.path.join(media.destination, song.filename)
    if not os.path.isfile(song_file):
        flash('No file for song with id {}'.format(song_id))
        return redirect(request.referer)
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referer)
    song.tags = []
    db.session.delete(song)
    db.session.commit()
    os.remove(song_file)
    flash('Deleted with id {}'.format(song_id))
    return redirect(request.referrer)


@client.route("/lastfm/search")
def last_fm_search():
    q = request.args.get('q')
    song_id = request.args.get('song_id')
    if not q:
        return redirect(request.referrer)
    track_results = search_last_fm(q)
    return render_template("last_fm_results.html",
                           q=q,
                           song_id=song_id,
                           track_results=track_results)


@client.route("/song/<int:song_id>/labels/add", methods=["post"])
def add_label_to_song(song_id):
    song = Song.query.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referrer)
    label_key = request.form.get('key')
    label_value = request.form.get('value')
    label_indexing = True if request.form.get('indexing') == "on" else False
    label = Label.create(key=label_key, value=label_value,
                         indexing=label_indexing)
    song.labels.append(label)
    db.session.add(song)
    db.session.commit()
    return redirect(url_for('listen', song_id=song.id))


# TODO
@client.route("/labels/edit", methods=["get", "post"])
def edit_label_key_value():
    if request.method == "GET":
        key = request.args.get("k")
        value = request.args.get("v")
        label_key_value_f = "{}: {}".format(key, value)
        label_results = {label_key_value_f: {
            "count": 0,
            "key": key,
            "value": value,
        }}
        if key and value:
            label_count = Label.query.filter_by(key=key, value=value).count()
            label_results[label_key_value_f]["count"] = label_count
        return render_template("label_edit.html", k=key, v=value,
                               label_results=label_results)
    elif request.method == "post":
        key = request.args.get("k")
        value = request.args.get("v")
        new_value = request.args.get("nv")
        if key and value:
            labels = Label.query.filter_by(key=key, value=value).all()


@client.route("/labels/search", methods=["get", "post"])
def search_label_key_value():
    if request.method == "GET":
        key = request.args.get("k")
        q = request.args.get("q")
        label_results = defaultdict(lambda: {
            "count": 0,
            "key": "",
            "value": "",
        })
        if key and q:
            labels = Label.query.whooshee_search(q).all()
            for label in labels:
                label_results["{}: {}".format(label.key, label.value)]["count"] += 1
                label_results["{}: {}".format(label.key, label.value)]["key"] = label.key
                label_results["{}: {}".format(label.key, label.value)]["value"] = label.value
        return render_template("label_search.html", k=key, q=q,
                               label_results=label_results)
    elif request.method == "post":
        key = request.args.get("k")
        q = request.args.get("q")
        new_value = request.args.get("nv")
        if key and q:
            labels = Label.query.filter_by(key=key, value=q).all()


@client.route("/labels/<int:label_id>/edit", methods=["post"])
def edit_label(label_id):
    label = Label.query.filter_by(id=label_id).first()
    if not label:
        flash('No label with id {}'.format(label_id))
        return redirect(request.referer)
    label.key = request.form.get('key')
    label.value = request.form.get('value')
    db.session.add(label)
    db.session.commit()
    return redirect(request.referrer)


@client.route("/labels/<int:label_id>/delete", methods=["post"])
def delete_label(label_id):
    label = Label.query.filter_by(id=label_id).first()
    if not label:
        flash('No label with id {}'.format(label_id))
        return redirect(request.referer)
    db.session.delete(label)
    db.session.commit()
    return redirect(request.referrer)


@client.route("/media/<path:filepath>")
def get_media(filepath):
    return send_from_directory(media.config.destination, filepath)


@client.route("/test-crudy")
def test_crudy():
    ctx = build_ctx()
    return render_template("test_crudy.html",
                           **ctx)


@client.route("/metronome/<int:song_id>")
def get_metronome(song_id):
    song = Song.query.filter_by(id=song_id).first()
    bpm_round = song.label_key("bpm_round")
    ctx = build_local_ctx()
    return render_template("metronome.html",
                           bpm_round=bpm_round,
                           song=song,
                           **ctx)
