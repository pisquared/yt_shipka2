"""
Define your models here, like so:

```
from shipka.store.search import whooshee

from shipka.store.database import db, ModelController, Datable, Ownable


@whooshee.register_model('body')
class BlogPost(db.Model, ModelController, Datable, Ownable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)
```
"""

from shipka.store.search import whooshee

from shipka.store.database import db, ModelController, Datable, Ownable





###################################################
#
# YT-specific models
#
###################################################




songs_tags = db.Table('songs_labels',
                      db.Column('song_id', db.Integer(), db.ForeignKey('song.id')),
                      db.Column('label_id', db.Integer(), db.ForeignKey('label.id')))


@whooshee.register_model('title', 'indexing_title')
class Song(db.Model, ModelController, Datable, Ownable):
    title = db.Column(db.Unicode)
    indexing_title = db.Column(db.Unicode)
    filename = db.Column(db.Unicode)
    duration = db.Column(db.Integer)

    default_start = db.Column(db.Integer)
    default_end = db.Column(db.Integer)

    labels = db.relationship("Label", secondary=songs_tags)

    @property
    def indexed_labels(self):
        return [label for label in self.labels if label.indexing]

    @property
    def non_indexed_labels(self):
        return [label for label in self.labels if not label.indexing]

    def label_key(self, label_key):
        for label in self.labels:
            if label.key == label_key:
                return label.value

    def label_keys(self, label_key):
        values = []
        for label in self.labels:
            if label.key == label_key:
                values.append(label.value)
        return values

    # def __repr__(self):
    #     return "<Song {}: '{}'>".format(self.id, self.title)


@whooshee.register_model('value')
class Label(db.Model, ModelController, Datable, Ownable):
    id = db.Column(db.Integer, primary_key=True)

    indexing = db.Column(db.Boolean)

    key = db.Column(db.Unicode)
    value = db.Column(db.Unicode)

    songs = db.relationship("Song", secondary=songs_tags)

    # def __repr__(self):
    #     return "<Label {}:{}>".format(self.key, self.value)